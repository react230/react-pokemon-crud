import React from 'react';
import '../assets/css/style.css';
const Col = (props) => {
    return (
        <div className={`col ${props?.className}`}>{props?.children}</div>
    );
};

export default Col;