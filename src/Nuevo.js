import React, { useState, useEffect } from 'react';
import './assets/css/style.css';
import Label from './components/Label';
import Row from './components/Row';
import Col from './components/Col';
import Input from './components/Input';
import Range from './components/Range';
import Button from './components/Button';
const Nuevo = (props) => {
    const [obj, setObj] = useState({ name: "", image: "", hp: 0, attack: 0, defense: 0 });
    const [isEdit, setEdit] = useState(false);

    const handleChange = (e) => {
        setEdit(true);
        setObj({ ...obj, [e.target.name]: e.target.value });
    }

    const buscarPokemonId = (id) => {
        props.peticionBackend(`pokemons/${id}`, {}, 'GET').then(dato => {
            setObj(dato);

        }).catch((error) => {
            props?.toggle();
        });
    }
    const editarPokemon = () => {
        props.peticionBackend(`pokemons/${obj.id}`, obj, 'PUT').then(dato => {
            props.toggle();
        }).catch((error) => {
            console.log("error: ", error)
        });
    }
    const nuevoPokemon = () => {
        var g = { ...obj, type: "normal", hp: 100, idAuthor: props.author };
        props.peticionBackend(`pokemons`, g, 'POST').then(dato => {
            props.toggle();
        }).catch((error) => {
            console.log("error: ", error)

        });
    }

    useEffect(() => {
        if (props?.obj?.id) {
            buscarPokemonId(props.obj.id)
        } else {
            setObj({ name: "", image: "", h1: 0, attack: 0, defense: 0 });
        }
    }
        , [props.obj]);


    return (
        <div className='card'>
            <Row>
                <Col className={"alignCenter"}>
                    <Label>Nuevo Pokemon</Label>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Row>
                        <Col className={"col-25 alignRight"}>
                            <Label>
                                Nombre:
                            </Label>
                        </Col>
                        <Col className={"col-75"}>
                            <Input
                                name="name"
                                onChange={(e) => {
                                    handleChange(e);
                                }} value={obj?.name} />
                        </Col>
                    </Row>
                    <Row>
                        <Col className={"col-25 alignRight"}>
                            <Label>
                                Imagen:
                            </Label>
                        </Col>
                        <Col className={"col-75"}>
                            <Input
                                name="image"
                                onChange={handleChange} value={obj?.image} />
                        </Col>
                    </Row>
                </Col>
                <Col>
                    <Row>
                        <Col className={"col-30 alignRight"}>
                            <Label>
                                Defensa:
                            </Label>
                        </Col>
                        <Col className={"col-05 alignCenter"}>
                            <Label>
                                0
                            </Label>
                        </Col>
                        <Col className={"col-60"}>
                            <Range
                                name={"defense"}
                                onChange={handleChange} value={obj?.defense} />
                        </Col>
                        <Col className={"col-05 alignCenter"}>
                            <Label>
                                100
                            </Label>
                        </Col>
                    </Row>
                    <Row>
                        <Col className={"col-30 alignRight"}>
                            <Label>
                                Ataque:
                            </Label>
                        </Col>
                        <Col className={"col-05 alignCenter"}>
                            <Label>
                                0
                            </Label>
                        </Col>
                        <Col className={"col-60"}>
                            <Range
                                name={"attack"}
                                onChange={handleChange} value={obj?.attack} />
                        </Col>
                        <Col className={"col-05 alignCenter"}>
                            <Label>
                                100
                            </Label>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row>
                <Col className={"alignCenter"}>
                    <Button className={`btn-primary ${isEdit ? '' : "btn-disabled"}`}
                        onClick={obj.hasOwnProperty('id') ? editarPokemon : nuevoPokemon}>
                        <span>
                            <svg className='icon' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
                                <path d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z" />
                            </svg>
                        </span>
                        &nbsp;
                        Guardar</Button>
                    &nbsp;
                    <Button className={'btn-primary'} onClick={props?.toggle?.bind(null)} >
                        <span>
                            <svg className='icon' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16">
                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                            </svg>
                        </span>
                        &nbsp;
                        Cancelar</Button>
                </Col>
            </Row>
        </div>
    );
};

export default Nuevo;