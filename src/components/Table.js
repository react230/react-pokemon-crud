import React,{useEffect, useState} from 'react';
import '../assets/css/style.css';


const Table = (props) => {
  const [rows, setRows] = useState([]);
  const [columns, setColumns] = useState([]);

  useEffect(() => {
    setRows(props?.rows?props.rows:[]);
    setColumns(props?.columns?props.columns:[]);
  }, [props]);

  return (
    <>
      <table style={{ width: '100%' }}>
        <tr>
          {columns.map(
            (prop, index) => {
              return <th key={"th-"+index}>{prop.Header}</th>
            }
          )}
        </tr>
        {rows.map((val, key) => {
          return (
            <tr key={key}>
              {columns.map((prop, index) => {
                return <td>{val[prop.accesor]}</td>
              })
              }
            </tr>
          )
        })}
      </table>
    </>
  );
};

export default Table;