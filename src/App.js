import React, { useEffect, useState } from 'react';

import logo from './logo.svg';
import './App.css';
import './assets/css/style.css';
import Button from './components/Button';
import Input from './components/Input';
import Label from './components/Label';
import Range from './components/Range';
import Table from './components/Table';
import Row from './components/Row';
import Col from './components/Col';
import Nuevo from './Nuevo';
const data = [
  { nombre: "Anom", imagen: 19, ataque: "Male", defensa: "Male", acciones: "Male" },
]
const baseUrl = "https://pokemon-pichincha.herokuapp.com/";
const columns = [
  {
    Header: "Nombre",
    accesor: "name"
  },
  {
    Header: "Imagen",
    accesor: "image"
  },
  {
    Header: "Ataque",
    accesor: "attack"
  },
  {
    Header: "Defensa",
    accesor: "defense"
  },
  {
    Header: "Acciones",
    accesor: "acciones"
  }
];
function App() {
  const [rows, setRows] = useState([]);
  const [obj, setObj] = useState({});
  const [nuevo, setNuevo] = useState(false);
  const [author, setAuthor] = useState('929792323');
  const handleChange = (e) => {
    // console.log(e.target);
  }
  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      buscarPokemon(event.target.value);
    }
  }
  async function peticionBackend(url = '', data = {}, method = 'POST') {
    var myHeaders = new Headers({
      'Content-Type': 'application/json',
    });
    const options = {
      method: method,
      headers: myHeaders
    };
    if (method == 'POST' || method == 'PUT') {
      options.body = JSON.stringify(data);
    }
    const response = await fetch(`${baseUrl}${url}`, options);
    return response.json();
  }

  const buscarPokemon = (name = "") => {
    peticionBackend(`pokemons/?idAuthor=${parseInt(author)}${name !== "" ? "&name=" + name : ""}`, {}, 'GET').then(dato => {
      var f = dato.map((item, i) => {
        return {
          ...item, image: (<div className='alignCenter'><img width={'50px'} height={'50px'} src={`${item.image}`}></img></div>), 
          acciones: <div className='alignCenter'>
            <Button onClick={() => {
              setObj(item);
              setNuevo(true);
            }
            }>
              <svg className='icon icon-primary' xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z" />
              </svg>

            </Button>
            <Button onClick={() => {
              eliminarPokemon(item.id);
            }
            }>
              <svg className={"icon icon-primary"} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z" />
              </svg>
            </Button>
          </div>
        }

      }
      );
      setRows(f);
    }).catch((error) => {
      console.log(error)

    }).then(() => {
    });
  }



  const eliminarPokemon = (id) => {
    peticionBackend(`pokemons/${id}`, obj, 'DELETE').then(dato => {
      buscarPokemon();
    }).catch((error) => {
      console.log("error: ", error)
    });
  }


  const toggleNuevo = () => {
    setNuevo(!nuevo);
    buscarPokemon();

  }

  useEffect(() => {
    buscarPokemon();
  }, []);

  return (

    <div className='container'>
      <Row>
        <Col>
          <Label>Listado de Pokemon</Label>
        </Col>
      </Row>
      <Row>
        <Col>
          <div class="parent">
            <div className={"input-wrapper"}>
              <Input onChange={handleChange} onKeyPress={handleKeyPress} icon={<svg xmlns="http://www.w3.org/2000/svg" class="input-icon" viewBox="0 0 20 20" fill="currentColor">
                <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd" />
              </svg>} placeholder={"Buscar"} />
            </div>
            <div className="child">
              <Button onClick={() => {
                setNuevo(true);
                setObj({});
              }} className={'btn-primary'} >
                <span >
                  <svg className='icon' xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" viewBox="0 0 16 16">
                    <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />
                  </svg>
                </span>
                <span>

                  Nuevo
                </span>

              </Button>
            </div>
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table rows={rows} columns={columns}></Table>
        </Col>
      </Row>
      <Row>
        <Col>
          {nuevo && <Nuevo peticionBackend={peticionBackend} author={author} toggle={toggleNuevo} obj={obj} />}
        </Col>
      </Row>

    </div>
  );
}

export default App;

// https://codepen.io/edulazaro/pen/ExVBKKb