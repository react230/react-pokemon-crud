import React from 'react';

const Range = (props) => {
    return (
        <input style={{width:'100%'}} onChange ={props?.onChange} name={props?.name} value={props?.value} type={'range'}></input>
    );
};

export default Range;