import { render, screen, waitFor } from '@testing-library/react';
import App from './App';
import Nuevo from './Nuevo';


test('renders Listado de Pokemon', () => {
  render(<App />);
  const linkElement = screen.getByText(/Listado de Pokemon/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders Nuevo Pokemon', () => {
  render(<Nuevo />);
  const linkElement = screen.getByText(/Nuevo Pokemon/i);
  expect(linkElement).toBeInTheDocument();
});


test('renders cargar Pokemon con id 7612', () => {
  render(<Nuevo id={7612} />);
  setTimeout(() => {
    screen.findAllByDisplayValue(/TortugaVerde/i);
    expect(linkElement).toBeInTheDocument();
  }, 1000);
});

