import { render, screen } from '@testing-library/react';
import Button from '../Button';
import Input from '../Input';
import Label from '../Label';
import Range from '../Range';
import Row from '../Row';
import Col from '../Col';
import Table from '../Table';
const columns = [
  {
    Header: "Nombre",
    accesor: "name"
  },
  {
    Header: "Imagen",
    accesor: "image"
  },
  {
    Header: "Ataque",
    accesor: "attack"
  },
  {
    Header: "Defensa",
    accesor: "defense"
  },
  {
    Header: "Acciones",
    accesor: "acciones"
  }
];
test('renders Button with children', () => {
    render(<Button>Prueba</Button>);
    const linkElement = screen.getByText(/Prueba/i);
  expect(linkElement).toBeInTheDocument();
  });
  
  test('renders Col with children', () => {
    render(<Col>Prueba</Col>);
    const linkElement = screen.getByText(/Prueba/i);
  expect(linkElement).toBeInTheDocument();
  });

  test('renders Input with value', () => {
    render(<Input value={"Hola"}/>);
    const linkElement = screen.getByDisplayValue(/Hola/i);
  expect(linkElement).toBeInTheDocument();
  });

  test('renders Label with children', () => {
    render(<Label>Prueba</Label>);
    const linkElement = screen.getByText(/Prueba/i);
  expect(linkElement).toBeInTheDocument();
  });

  test('renders Row with children', () => {
    render(<Row>Prueba</Row>);
    const linkElement = screen.getByText(/Prueba/i);
  expect(linkElement).toBeInTheDocument();
  });

  test('renders Table with children', () => {
    render(<Table columns={columns}></Table>);
    const linkElement = screen.getByText(/Nombre/i);
  expect(linkElement).toBeInTheDocument();
  });