import React from 'react';
import '../assets/css/style.css';
const Row = (props) => {
    return (
        <div className='row'>{props?.children}</div>
    );
};

export default Row;