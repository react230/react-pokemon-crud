import React from 'react';
import '../assets/css/style.css';
const Input = (props) => {
    return (
        <>
            <input value={props?.value} name={props?.name} onChange={props?.onChange} onKeyPress={props?.onKeyPress} className={(props?.icon) ? "input input-1" :"input"} placeholder={props?.placeholder} />
            {props?.icon}
        </>
    );
};

export default Input;